﻿using System.Numerics;

namespace RVC
{
    // The following logic applies.
    // - The room has knowledge of its own size and what objects are in it
    // - A Vacuum askes the room if it can be placed at a desired position
    // - A vacuum can rotate unrestricted but has to ask the room to see if it can move
    // - A Vacuum does not know its own position but has to ask the room for it
    // - A Vacuum does know of its own rotation

    internal class Floor
    {
        public Floor(float x, float y)
        {
            m_size = new(x, y);
        }

        private bool PositionIsOnFloor(Vector2 position)
        {
            // Bare in mind the size e.g. 3,3 of the floor means
            // the positin range is within 0,0 and 2,2
            return ((position.X < 0 || position.X >= m_size.X)
                || (position.Y < 0 || position.Y >= m_size.Y));
        }

        public bool PlaceRobot(Vacuum vacuum, Vector2 position)
        {
            if (!PositionIsOnFloor(position))
            {
                return false;
            }
            // Check if vacuum is already on the floor
            if (m_vacuums.ContainsKey(vacuum))
            {
                return false;
            }
            // Check if the position is occupied
            // (Could be extended with other objects other than Vacuums)
            else if (m_vacuums.ContainsValue(position))
            {
                return false;
            }

            m_vacuums[vacuum] = position;
            return true;
        }

        public bool MoveRobot(Vacuum vacuum, Vector2 relativeMove)
        {
            if (!m_vacuums.TryGetValue(vacuum, out var position))
            {
                return false;
            }

            var newPosition = position + relativeMove;
            if (m_vacuums.ContainsValue(newPosition))
            {
                return false;
            }

            m_vacuums[vacuum] = newPosition;
            return true;
        }

        public Vector2 GetRobotPosition(Vacuum vacuum) => m_vacuums[vacuum];

        private Vector2 m_size;

        private readonly Dictionary<Vacuum, Vector2> m_vacuums = new();
    }

    internal class Vacuum
    {
        public enum Movement
        {
            A,
            W,
            S,
            D,
        }

        public enum Orientation
        {
            W,
            N,
            E,
            S,
        }

        public Vacuum(Floor floor)
        {
            m_floor = floor;
        }

        public bool Placed { private set; get; } = false;

        public bool Move(Movement move)
        {
            // Rotation is always allowed as it keeps the position
            switch (move)
            {
                // Rotate CCW
                case Movement.A:
                    m_orientation = new(-m_orientation.Y, m_orientation.X);
                    return true;
                // Rotate CW
                case Movement.D:
                    m_orientation = new(m_orientation.Y, -m_orientation.X);
                    return true;
                // Forward
                case Movement.W:
                    return m_floor.MoveRobot(this, m_orientation);
                // Backward
                case Movement.S:
                    return m_floor.MoveRobot(this, -m_orientation);
            }
            return false;
        }

        public bool Place(Vector2 position, Orientation orientation)
        {
            if (Placed)
            {
                // already placed
                return false;
            }

            bool success = m_floor.PlaceRobot(this, position);
            if (success)
            {
                Placed = true;
                m_orientation = m_orientationToVector[orientation];
            }
            return success;
        }

        public Orientation GetOrientation() => m_orientationToVector.FirstOrDefault(x => x.Value == m_orientation).Key;

        public (float, float) GetPosition()
        {
            var pos = m_floor.GetRobotPosition(this);
            return (pos.X, pos.Y);
        }

        private Floor m_floor;
        private Vector2 m_orientation;

        private Dictionary<Orientation, Vector2> m_orientationToVector = new()
        {
            { Orientation.W, new(-1, 0) },
            { Orientation.N, new(0, 1) },
            { Orientation.E, new(1, 0) },
            { Orientation.S, new(0, -1) },
        };
    }
}
